Partner: Zak Olyarnik

Lab3 (Lab_3.java):

This file works by creating 2 onClickListeners, one for the 9 buttons that represent the tic tac toe board (move), and one to restart the game (newgame).
Restart sets the board to all of its default values.  The other onclick listeners set the value of the text on the buttons to "X" or "O" and
determine if the match has ended.  The listeners call a "check()" function that determines if a combination of X's or O's has resulted in a win.
It returns "X" if X has won, "O" if O won, or "" if none have won yet.

meu_home__screen.xml:

This file goes into the menu folder under src/main/res/menu

activity_home__screen.xml:

This file goes into the layout folder under src/main/res/layout