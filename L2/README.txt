Partner: Zak_Olyarnik

Part 1 (Calendar_no_temboo):

Accesses the Google Calendar API without using Temboo.  After we get through the OAuth request and have the user's token,
we set up the url "https://www.googleapis.com/calendar/v3/users/me/calendarList?access_token=" to get the client's 
calendar ID.  After thats been taken care of, we use that ID in conjunction with the access token in this url,
"https://www.googleapis.com/calendar/v3/calendars/" + id + "/events?access_token=" + access_token.  This gives
us access to the user's calendar so we can pull information from their calendar.

Part 2 (Calendar):

It produces the same result as above, but with using temboo.  However, this time we ask for the user's temboo
account, app Name, and they're app's key.  When we want to retrieve information from the user's Calendar,
we create a new temboo session and user requests such as "GetAllCalendars".  We get the calendar ID from
"GetAllCalendars".  Then we start another temboo session to get all of the Calendar's events using 
"GetAllEvents".  Once we get the JSON from that, we parse it for the date and/or time.