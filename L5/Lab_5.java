import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import com.google.gson.JsonArray;
// Requires gson jars and temboo jar
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.CloudMine.UserAccountManagement.AccountLogin;
import com.temboo.Library.CloudMine.UserAccountManagement.AccountLogin.AccountLoginInputSet;
import com.temboo.Library.CloudMine.UserAccountManagement.AccountLogin.AccountLoginResultSet;
import com.temboo.Library.Google.Calendar.GetAllCalendars;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsInputSet;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsResultSet;
import com.temboo.Library.Google.Calendar.GetAllEvents;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsInputSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsResultSet;
import com.temboo.core.TembooSession;

public class Lab_5 {

	//handles google related operations for both users
	static GoogleCal googleCal = new GoogleCal();
	
	//handles Cloudmine operations
	static Cloudmine cloud = new Cloudmine();
	
		public static void main(String[] args) throws Exception {

			String acctName, appName, appKeyValue;
			//ClientID, ClientSecret, and redirect are all supplied by the user.  The "id" is the calender id
			String clientID = "", clientSecret = "", redirect = "";
			
			//arrayOfKeys contains the names of all users after .split() has been called.  It is then converted
			//into an ArrayList so the ".contains" method can be used
			String[] arrayOfKeys;
			ArrayList<String> listOfKeys = new ArrayList<String>();
			
			
			//This file is needed to store the key value of the file in Cloudmine
			File userKey = new File("fileKeyValue.txt");
			
			//If the file exists and isn't empty, read from it.  Reading from an empty file caused issues
			if(userKey.exists() && userKey.length() != 0)
			{
				//Gets the usernames as a single string, split them on the the spaces, and convert it to an ArrayList
				BufferedReader brKey = new BufferedReader(new FileReader (userKey));
				String line = brKey.readLine();
				arrayOfKeys = line.split(" ");
				listOfKeys = new ArrayList<String>(Arrays.asList(arrayOfKeys));
				brKey.close();
			}
			
			//userKeyID refers to the usernames of the two people using this program.  The names refer to the
			//keys of the files stored in Cloudmine
			String userKeyID, userKeyID2;
			
			//retrieves necessary info from the user in order to check if they already have an account
			Scanner in = new Scanner(System.in);
			System.out.println("Enter the first person's username: ");
			userKeyID = in.nextLine();
			userKeyID = userKeyID.replaceAll(" ", "_");
			
			System.out.println("Enter the second person's username: ");
			userKeyID2 = in.nextLine();
			userKeyID2 = userKeyID2.replaceAll(" ", "_");
			
			//Where temboo information is to be entered
			System.out.println("Enter Temboo Account Name");
			acctName = in.nextLine();
			System.out.println("Enter Temboo App Name");
			appName = in.nextLine();
			System.out.println("Enter Temboo App Key");
			appKeyValue = in.nextLine();
			
			
			//where Cloudmine info is to be entered
			System.out.println("Enter Cloudmine Account Name");
			String Username = in.nextLine();
			System.out.println("Enter Cloudmine Password");
			String Password = in.nextLine();
			System.out.println("Enter Cloudmine API Key");
			String APIkey = in.nextLine();
			System.out.println("Enter Cloudmine Application Identifier");
			String AppIdentifier = in.nextLine();
			
			initializeGoogleCal(acctName, appName, appKeyValue);
			
			//Get permission from Cloudmine
			cloud.initialize(acctName, appName, appKeyValue);
			String access = cloud.logIn(APIkey, Username, Password, AppIdentifier);

			String[] myInfo;
			String[] friendInfo;
			//if a key for Cloudmine has not already been made
			if(!(listOfKeys.contains(userKeyID)) && !(listOfKeys.contains(userKeyID2)))
			{
				//Information needed to do the google oauth process
				System.out.println("Enter Google Client ID");
				clientID = in.nextLine();
				System.out.println("Enter Google Client Secret");
				clientSecret = in.nextLine();
				System.out.println("Enter Google Redirect URI");
				redirect = in.nextLine();

				//Creates two new users
				myInfo = executeGoogleGetTime(clientID, clientSecret, redirect, APIkey, Password, AppIdentifier, userKeyID);
				friendInfo = executeGoogleGetTime(clientID, clientSecret, redirect, APIkey, Password, AppIdentifier, userKeyID2);
			
			}
			else if(!(listOfKeys.contains(userKeyID)))
			{
				//Information needed to do the google oauth process
				System.out.println("Enter Google CLient ID");
				clientID = in.nextLine();
				System.out.println("Enter Google Client Secret");
				clientSecret = in.nextLine();
				System.out.println("Enter Google Redirect URI");
				redirect = in.nextLine();
				
				//creates one new user if the second user already exists.  Cloudmine is used for the other user if they exist
				myInfo = executeGoogleGetTime(clientID, clientSecret, redirect, APIkey, Password, AppIdentifier, userKeyID);
				friendInfo = executeCloudmineGetTime(APIkey, AppIdentifier, userKeyID2);
			}
			else if(!(listOfKeys.contains(userKeyID2)))
			{
				//Information needed to do the google oauth process
				System.out.println("Enter Google CLient ID");
				clientID = in.nextLine();
				System.out.println("Enter Google Client Secret");
				clientSecret = in.nextLine();
				System.out.println("Enter Google Redirect URI");
				redirect = in.nextLine();
				
				//Creates a new user if they haven't been made.  Cloudmine is used for the other user if they exist
				myInfo = executeCloudmineGetTime(APIkey, AppIdentifier, userKeyID);
				friendInfo = executeGoogleGetTime(clientID, clientSecret, redirect, APIkey, Password, AppIdentifier, userKeyID2);
			}
			else
			{
				myInfo = executeCloudmineGetTime(APIkey, AppIdentifier, userKeyID);
				friendInfo = executeCloudmineGetTime(APIkey, AppIdentifier, userKeyID2);
			}

		    // reads in date in a normal format
		    System.out.println("\nEnter date to compare (dd/mm/yyyy)");
		    String compareDate = in.nextLine();
		        
		    String[] myBusySchedule = googleCal.compareEvents(myInfo[3], myInfo[1], myInfo[0], myInfo[2], compareDate);
			String[] friendBusySchedule = googleCal.compareEvents(myInfo[3], friendInfo[1], friendInfo[0], friendInfo[2], compareDate);

			// prints a list of times that are still free for both users
		    for (int i = 0; i < 24; i++){
		    	if(myBusySchedule[i].equals("free") && friendBusySchedule[i].equals("free")){
		        	System.out.println(i + ":00:00 - free");
		        }
		    }
			          
		}

		
		//initializes googleCal with the neccessary temboo information to complete the google temboo operations
		public static void initializeGoogleCal(String acctName, String appName, String appKeyValue)
		{
			googleCal.initialize(acctName, appName, appKeyValue);
		}
		
		//This text would be repeated in the main body many times, so I put it in its own function so it can just be called
		//It gets the refreshToken, since refreshToken's last longer than regular access tokens.
		//It uses that token to get the calender id from the user, and then access the user's calender with that id
		//Finally, it uploads the the user's name and information into Cloudmine
		
		//This function is used if a person does not already have a file stored in Cloudmine with their information
		public static String[] executeGoogleGetTime(String clientID, String clientSecret, String redirect, String APIkey, String Password, String AppIdentifier,
				String userKeyID) throws Exception
		{
			//get the google information from the first user user
			String refresh_token = googleCal.getAccessToken(clientID, redirect, clientSecret);
			
			//The getAllCalendarResults choreo is used to get the Calendar id of the user so we can use it to
			//search the calendar
		
			String id = googleCal.getID(clientSecret, refresh_token, clientID);
	
			//The spaces serve as an indicator for the split() function in the future
			String users = clientSecret + " " + refresh_token + " " + clientID + " " + id;

			cloud.upload(users, APIkey, Password, AppIdentifier, userKeyID);
	
			//Contains an array of dates and an array of times
			googleCal.getTimes(id, refresh_token, clientID, clientSecret);
			String[] clientInfo = new String[]{refresh_token, id, clientID, clientSecret};
			
			return clientInfo;
		}
		
		//If a user already has their Google information stored in Cloudmine, then retrieve that information,
		//and retrieve the date and times of events from that user's calender
		
		public static String[] executeCloudmineGetTime(String APIkey, String AppIdentifier, String userKeyID) throws Exception
		{
			String userInfo = cloud.retrieveFile(APIkey, AppIdentifier, userKeyID);
			String[] userInfoArray = userInfo.split(" ");
			
			String clientSecret = userInfoArray[0];
			String refresh_token = userInfoArray[1];
			String clientID = userInfoArray[2];
			String id = userInfoArray[3];
			
			//Gets the date and times of events
			googleCal.getTimes(id, refresh_token, clientID, clientSecret);
			
			//returns a String array containing the calender id and refresh_token to be used in the main function
			String[] clientInfo = new String[]{refresh_token, id, clientID, clientSecret};
			
			return clientInfo;
		}

}