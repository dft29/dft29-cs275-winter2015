//Dana Thompson
//Handles all operations related to Cloudmine

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.codec.binary.Base64;

import com.google.gson.JsonArray;
// Requires gson jars and temboo jar
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.CloudMine.FileStorage.DeleteFile;
import com.temboo.Library.CloudMine.FileStorage.DeleteFile.DeleteFileInputSet;
import com.temboo.Library.CloudMine.FileStorage.DeleteFile.DeleteFileResultSet;
import com.temboo.Library.CloudMine.FileStorage.GetFile;
import com.temboo.Library.CloudMine.FileStorage.GetFile.GetFileInputSet;
import com.temboo.Library.CloudMine.FileStorage.GetFile.GetFileResultSet;
import com.temboo.Library.CloudMine.FileStorage.SetFile;
import com.temboo.Library.CloudMine.FileStorage.SetFile.SetFileInputSet;
import com.temboo.Library.CloudMine.FileStorage.SetFile.SetFileResultSet;
import com.temboo.Library.CloudMine.UserAccountManagement.AccountLogin;
import com.temboo.Library.CloudMine.UserAccountManagement.AccountLogin.AccountLoginInputSet;
import com.temboo.Library.CloudMine.UserAccountManagement.AccountLogin.AccountLoginResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;


public class Cloudmine {
	
	//Stores the temboo information for multiple uses
	public String acctName_ = "";
	public String appName_ = "";
	public String appKeyValue_ = "";
	
	//set the public variables.  They are used in all of the temboo sessions
	public void initialize(String acctName, String appName, String appKeyValue)
	{
		acctName_ = acctName;
		appName_ = appName;
		appKeyValue_ = appKeyValue;
	}
	
	//Get permission to use the cloudmine account
	public String logIn(String APIkey, String Username, String Password, String AppIdentifier) throws Exception
	{
		TembooSession session = new TembooSession(acctName_, appName_, appKeyValue_);
		AccountLogin accountLoginChoreo = new AccountLogin(session);

		// Get an InputSet object for the choreo
		AccountLoginInputSet accountLoginInputs = accountLoginChoreo.newInputSet();

		// Set inputs
		accountLoginInputs.set_APIKey(APIkey);
		accountLoginInputs.set_Username(Username);
		accountLoginInputs.set_Password(Password);
		accountLoginInputs.set_ApplicationIdentifier(AppIdentifier);

		// Execute Choreo
		AccountLoginResultSet accountLoginResults = accountLoginChoreo.execute(accountLoginInputs);
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(accountLoginResults.get_Response());
		JsonObject rootobj = root.getAsJsonObject();
		
		String session_token_ = rootobj.get("session_token").getAsString();
		
		return session_token_;

	}
	
	//upload the user's Google client id, access token, client secret, and calander id.  All of that information
	//is in the string user1
	public void upload(String user1, String APIkey, String Password, String AppIdentifier, String keyID) throws Exception
	{
		byte[] encoded = Base64.encodeBase64(user1.getBytes());
		
		TembooSession session = new TembooSession(acctName_, appName_, appKeyValue_);
		SetFile setFileChoreo = new SetFile(session);

		// Get an InputSet object for the choreo
		SetFileInputSet setFileInputs = setFileChoreo.newInputSet();

		// Set inputs
		setFileInputs.set_APIKey(APIkey);
		setFileInputs.set_ApplicationIdentifier(AppIdentifier);
		setFileInputs.set_FileContents(new String(encoded));
		setFileInputs.set_ContentType(".txt");
		setFileInputs.set_Key(keyID);

		// Execute Choreo
		SetFileResultSet setFileResults = setFileChoreo.execute(setFileInputs);
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(setFileResults.get_Response());
		JsonObject rootobj = root.getAsJsonObject();
		
		String key = rootobj.get("key").getAsString();
		
		File userBase = new File("fileKeyValue.txt");
		String userBaseFileName = userBase.getName();
		String fileContents = "";
		
		//If the file exists and is not empty, read from it.  Issues arose when the file existed, but was empty
		if(userBase.length() != 0 && userBase.exists())
		{
			//Gets the contents of the file so that information can be added, instead of overwritten
			BufferedReader brKey = new BufferedReader(new FileReader (userBaseFileName));
			fileContents = brKey.readLine();
			brKey.close();
		}
		
		//If the user does not already exist as a valid key, add them
		if(!fileContents.contains(key))
		{
			fileContents += key + " ";
			PrintWriter writer = new PrintWriter(userBaseFileName, "UTF-8");
			writer.println(fileContents);
			writer.close();
		}
	}
	
	//Retrieves the file contents from CloudMine
	public String retrieveFile(String appKey, String appId, String fileKey) throws Exception
	{
		
		TembooSession session = new TembooSession(acctName_, appName_, appKeyValue_);
		GetFile getFileChoreo = new GetFile(session);

		// Get an InputSet object for the choreo
		GetFileInputSet getFileInputs = getFileChoreo.newInputSet();

		// Set inputs
		getFileInputs.set_APIKey(appKey);
		getFileInputs.set_Key(fileKey);
		getFileInputs.set_ApplicationIdentifier(appId);

		// Execute Choreo
		GetFileResultSet getFileResults = getFileChoreo.execute(getFileInputs);
		String content = getFileResults.get_Response();
		
		return content;
	}
	
}
