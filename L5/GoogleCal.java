//Dana Thompson
//This class handles all Google related functions, such as id retrieval, connection, etc.

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.google.gson.JsonArray;
// Requires gson jars and temboo jar
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Google.Calendar.GetAllCalendars;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsInputSet;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsResultSet;
import com.temboo.Library.Google.Calendar.GetAllEvents;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsInputSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsResultSet;
import com.temboo.Library.Google.Calendar.SearchEvents;
import com.temboo.Library.Google.Calendar.SearchEvents.SearchEventsInputSet;
import com.temboo.Library.Google.Calendar.SearchEvents.SearchEventsResultSet;
import com.temboo.core.TembooSession;


public class GoogleCal {
	
	//Stores temboo information for multiple uses
	public String acctName_ = "";
	public String appName_ = "";
	public String appKeyValue_ = "";
	
	//Set the first three temboo variables
	public void initialize(String acctName, String appName, String appKeyValue)
	{
		acctName_ = acctName;
		appName_ = appName;
		appKeyValue_ = appKeyValue;
	}
	
	//Get access to Google calenders
	public String getAccessToken(String clientID, String redirect, String clientSecret) throws Exception
	{
		Scanner in = new Scanner(System.in);
		//  Let Temboo do the front end Oauth work!
		//  1st - the initial OAuth step
		String oauthURL = "https://accounts.google.com/o/oauth2/auth?access_type=offline&client_id=" + clientID + "&scope=https://www.googleapis.com/auth/calendar&response_type=code&redirect_uri=" + redirect + "&state=/profile&approval_prompt=force";
		System.out.println("Go to the following URL and obtain the code that you find there.\n" + oauthURL);
		String code = in.nextLine();
	 
		// Google requires a POST for the next step
		String authorizeURL = "https://accounts.google.com/o/oauth2/token";
		String authorizeParams = "code=" + code + "&client_id=" + clientID + "&client_secret=" + clientSecret + "&redirect_uri=" + redirect + "&grant_type=authorization_code";
		String authorizeResponse = executePost(authorizeURL, authorizeParams); 

		JsonParser oauth_jp = new JsonParser();
		JsonElement oauth_root = oauth_jp.parse(authorizeResponse);
		JsonObject oauth_rootobj = oauth_root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive
		String refresh_token = oauth_rootobj.get("refresh_token").getAsString();
		
		return refresh_token;
	}
	
	//Get the user's calender ID
	public String getID(String clientSecret, String refresh_token, String clientID) throws Exception
	{
		TembooSession session = new TembooSession(acctName_, appName_, appKeyValue_);
		GetAllCalendars getAllCalendarsChoreo = new GetAllCalendars(session); 

		// Get an InputSet object for the choreo
		GetAllCalendarsInputSet getAllCalendarsInputs = getAllCalendarsChoreo.newInputSet();

		// Set inputs
		getAllCalendarsInputs.set_ClientSecret(clientSecret);
		getAllCalendarsInputs.set_RefreshToken(refresh_token);
		getAllCalendarsInputs.set_ClientID(clientID);

		// Execute Choreo
		GetAllCalendarsResultSet getAllCalendarsResults = getAllCalendarsChoreo.execute(getAllCalendarsInputs);
  
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(getAllCalendarsResults.get_Response());
		JsonObject rootobj = root.getAsJsonObject();
   
		JsonArray items = rootobj.get("items").getAsJsonArray();
		JsonObject item = items.get(0).getAsJsonObject();
   
		//Needed in order to gain access to the users Calendar
		String id = item.get("id").getAsString();
		
		return id;
	}
	
	//Get the user's calander info and save it in two String lists
	public void getTimes(String ID, String refresh, String clientID, String clientSecret) throws Exception
	{
		TembooSession session = new TembooSession(acctName_, appName_, appKeyValue_);                               
		GetAllEvents getAllEventsChoreo = new GetAllEvents(session);

		// Get an InputSet object for the choreo
		GetAllEventsInputSet getAllEventsInputs = getAllEventsChoreo.newInputSet();

		// Set inputs
		getAllEventsInputs.set_ClientID(clientID);
		getAllEventsInputs.set_ClientSecret(clientSecret);
		getAllEventsInputs.set_RefreshToken(refresh);
		getAllEventsInputs.set_CalendarID(ID); 

		// Execute Choreo
		GetAllEventsResultSet getAllEventsResults = getAllEventsChoreo.execute(getAllEventsInputs);       
                          
		// Now parse the json
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(getAllEventsResults.get_Response());
		JsonObject rootobj = root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive
   
		//Prints out the date, or the time and date of an event if one is set
		JsonArray items = rootobj.get("items").getAsJsonArray();
		System.out.println("\n");
		
		//print out the events of the user's calendar
		for(int i = 0; i < items.size(); i++) 
		{
			JsonObject item = items.get(i).getAsJsonObject();
			String timex;
			JsonObject start = item.get("start").getAsJsonObject();
			if(start.has("dateTime"))
			{
				timex=start.get("dateTime").getAsString();
			}
			else
			{
				timex=start.get("date").getAsString();
			}
			String title = item.get("summary").getAsString();
			System.out.println(timex + " - " + title);
		}
		
	}
	
	//Looks at all the events of a person's calender for one whole day, and figures out when that person is busy
	public String[] compareEvents(String gSecret, String gCalendar, String gRefresh, String gID, String compareDate) throws Exception
	{
		String startTime, endTime;
		JsonParser jp;
        JsonElement root;
        JsonObject rootobj;
        
		TembooSession session = new TembooSession(acctName_, appName_, appKeyValue_);

		//reformats the string for google
        String compareArray[] = compareDate.split("/");
        compareDate = compareArray[2] + "-" + compareArray[1] + "-" + compareArray[0];
        
        // arrays for each user's free times
        String meFree[] = new String[24];
        for (int i = 0; i < 24; i++){
        	meFree[i] = "free";
        }
        
        // checking first user's free times, broken up into sections to handle leading zeroes
        // and the last hour
        int k = 0;
        while (k < 9){
        	startTime = compareDate + "T0" + Integer.toString(k) + ":00:00-04:00";
        	endTime = compareDate + "T0" + Integer.toString(k+1) + ":00:00-04:00";
        	SearchEvents searchEventsChoreo = new SearchEvents(session);
            SearchEventsInputSet searchEventsInputs = searchEventsChoreo.newInputSet();
            searchEventsInputs.set_ClientSecret(gSecret);
            searchEventsInputs.set_CalendarID(gCalendar);
            searchEventsInputs.set_MinTime(startTime);
            searchEventsInputs.set_RefreshToken(gRefresh);
            searchEventsInputs.set_MaxTime(endTime);
            searchEventsInputs.set_ClientID(gID);
            SearchEventsResultSet searchEventsResults = searchEventsChoreo.execute(searchEventsInputs);
        	
            // Now parse the Json
            jp = new JsonParser();
            root = jp.parse(searchEventsResults.get_Response());
            rootobj = root.getAsJsonObject();
            
            JsonArray events = rootobj.get("items").getAsJsonArray();
            if (!(events.size() == 0)){
            	meFree[k] = "busy";
            }
            k++;
        }
        
        k = 9;
        while (k < 10){
	        startTime = compareDate + "T09:00:00-04:00";
	    	endTime = compareDate + "T10:00:00-04:00";
	    	SearchEvents searchEventsChoreo = new SearchEvents(session);
	        SearchEventsInputSet searchEventsInputs = searchEventsChoreo.newInputSet();
	        searchEventsInputs.set_ClientSecret(gSecret);
	        searchEventsInputs.set_CalendarID(gCalendar);
	        searchEventsInputs.set_MinTime(startTime);
	        searchEventsInputs.set_RefreshToken(gRefresh);
	        searchEventsInputs.set_MaxTime(endTime);
	        searchEventsInputs.set_ClientID(gID);
	        SearchEventsResultSet searchEventsResults = searchEventsChoreo.execute(searchEventsInputs);
	    	
	        // Now parse the Json
	        jp = new JsonParser();
	        root = jp.parse(searchEventsResults.get_Response());
	        rootobj = root.getAsJsonObject();
	        
	        JsonArray events = rootobj.get("items").getAsJsonArray();
	        if (!(events.size() == 0)){
            	meFree[k] = "busy";
            }
            k++;
        }
        
        k = 10;
        while (k < 23){
        	startTime = compareDate + "T" + Integer.toString(k) + ":00:00-04:00";
        	endTime = compareDate + "T" + Integer.toString(k+1) + ":00:00-04:00";
        	SearchEvents searchEventsChoreo = new SearchEvents(session);
            SearchEventsInputSet searchEventsInputs = searchEventsChoreo.newInputSet();
            searchEventsInputs.set_ClientSecret(gSecret);
            searchEventsInputs.set_CalendarID(gCalendar);
            searchEventsInputs.set_MinTime(startTime);
            searchEventsInputs.set_RefreshToken(gRefresh);
            searchEventsInputs.set_MaxTime(endTime);
            searchEventsInputs.set_ClientID(gID);
            SearchEventsResultSet searchEventsResults = searchEventsChoreo.execute(searchEventsInputs);
        	
            // Now parse the Json
            jp = new JsonParser();
            root = jp.parse(searchEventsResults.get_Response());
            rootobj = root.getAsJsonObject();
            
            JsonArray events = rootobj.get("items").getAsJsonArray();
            if (!(events.size() == 0)){
            	meFree[k] = "busy";
            }
            k++;
        }
        
        k = 23;
        while (k < 24){
        	startTime = compareDate + "T23:00:00-04:00";
        	endTime = compareDate + "T23:30:00-04:00";
        	SearchEvents searchEventsChoreo = new SearchEvents(session);
            SearchEventsInputSet searchEventsInputs = searchEventsChoreo.newInputSet();
            searchEventsInputs.set_ClientSecret(gSecret);
            searchEventsInputs.set_CalendarID(gCalendar);
            searchEventsInputs.set_MinTime(startTime);
            searchEventsInputs.set_RefreshToken(gRefresh);
            searchEventsInputs.set_MaxTime(endTime);
            searchEventsInputs.set_ClientID(gID);
            SearchEventsResultSet searchEventsResults = searchEventsChoreo.execute(searchEventsInputs);
        	
            // Now parse the Json
            jp = new JsonParser();
            root = jp.parse(searchEventsResults.get_Response());
            rootobj = root.getAsJsonObject();
            
            JsonArray events = rootobj.get("items").getAsJsonArray();
            if (!(events.size() == 0)){
            	meFree[k] = "busy";
            }
            k++;
        }
        return meFree;
	}
	
	
	//Needed to get authorization from Google
	public static String executePost(String targetURL, String urlParameters)
	{
		URL url;
		HttpURLConnection connection = null;  
		try {
			//Create connection
			url = new URL(targetURL);
			connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", 
					"application/x-www-form-urlencoded");

			connection.setRequestProperty("Content-Length", "" + 
					Integer.toString(urlParameters.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");  

			connection.setUseCaches (false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			//Send request
			DataOutputStream wr = new DataOutputStream (
					connection.getOutputStream ());
			wr.writeBytes (urlParameters);
			wr.flush ();
			wr.close ();

			//Get Response
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer(); 
			while((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			return response.toString();

		} catch (Exception e) {

			e.printStackTrace();
			return null;

		} finally {

			if(connection != null) {
				connection.disconnect(); 
			}
		}
	}
}
