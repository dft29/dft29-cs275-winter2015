Dana Thompson: dft29
Zak Olyarnik: zwo24

Lab_5.java:

-The main file where both class Cloudmine and GoogleCal are used
-functions executeGoogleGetTime and executeCloudmineGetTime serve to organize the code in a neat block
-executeGoogleGetTime performs the oauth process, as well as getting the calender id and the times of the user's events
-executeCloudmineGetTime does the same thing as the function above, except it skips the oauth process
-Two global variables, cloud and googleCal are used to perform the methods needed to retrieve file information
calendar ids, etc.
-Lastly, it checks the string arrays returned by googleCal's compareEvents method to see if both users are free
at a certain time

Cloudmine.java:

-initialize: serves to store temboo's information as variables to use in other functions
-logIn: serves to gain access to the database
-upload: upload a user's calender id, clientID, client secret, and redirect uri to cloudmine
-retrieveFile: gets the contents from the file specified by a key

GoogleCal.java:

-initialize: serves to store temboo's information as variables to use in other functions
-getAccessToken: gets the refresh_token from the oauth process in order to use in other temboo calls
-getID: get the calender id of a user and return it
-getTimes: print's out all of the events of a user and when they occur
-compareEvents: finds out whether the user is busy at any given hour during a date specified by the user