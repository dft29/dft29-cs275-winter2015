package com.example.dana.gmaps;
//Is responsible for reading in the csv file
//This csv_reader is not mine, or anyone elses

/**
 * Created by Dana on 2/28/2015.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CSV_Reader {
    InputStream inputStream;

    public CSV_Reader(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    //Returns a list of all stations listed in the CSV file
    public List<String> read() {
        //The stations will be stored in this list
        List<String> resultList = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            //reads the file line by line and stores station names
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                String[] row = csvLine.split(",");
                resultList.add(row[1]);
            }
        } catch (IOException ex) {
            throw new RuntimeException();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                throw new RuntimeException();
            }
        }
        return resultList;
    }
}