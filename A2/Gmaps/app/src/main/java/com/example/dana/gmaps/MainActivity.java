package com.example.dana.gmaps;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class MainActivity extends ActionBarActivity {

    String[] stationN;
    Spinner dropdownSource;
    Spinner dropdownDest;
    String[] trains;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button button01 = (Button) findViewById(R.id.button);
        final Button buttonMap = (Button) findViewById(R.id.button2);

        //Reads the CSV File and gets the station names
        InputStream inputStream = getResources().openRawResource(R.raw.station_id_name);
        CSV_Reader csvFile = new CSV_Reader(inputStream);
        List <String>Stations = csvFile.read();

        //Converts the String List into an array of Strings.  Needed in order to populate spinners
        stationN = Stations.toArray(new String[Stations.size()]);

        //Populates the 2 spinners with the station names
        dropdownSource = (Spinner)findViewById(R.id.spinner1);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stationN);
        dropdownSource.setAdapter(adapter);

        dropdownDest = (Spinner)findViewById(R.id.spinner2);
        ArrayAdapter<String> adapterDestination = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stationN);
        dropdownDest.setAdapter(adapterDestination);


        //When button is pressed, get the train number, arrival, and departure times and display them in a list
        View.OnClickListener find = new View.OnClickListener() {
            public void onClick(View v) {

                Spinner spinnerTop = (Spinner) findViewById(R.id.spinner1);
                String textTop = spinnerTop.getSelectedItem().toString();
                Spinner spinnerBottom = (Spinner) findViewById(R.id.spinner2);
                String textBottom = spinnerBottom.getSelectedItem().toString();

                ListView listView = (ListView) findViewById(R.id.mainListView);

                //If the source and destination station are the same, don't display a listView or map button
                //Don't want the user to click the button when there are invalid coordinates
                if (textTop.equals(textBottom)) {
                    buttonMap.setVisibility(View.INVISIBLE);
                    listView.setVisibility(View.INVISIBLE);
                } else {
                    trainList listTrains = new trainList();
                    listTrains.execute();
                }
            }
        };

        //When clicked, display the google map
        final Context context = this;
        buttonMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intention = new Intent(context, com.example.dana.gmaps.MapsActivity.class);
                intention.putExtra("trainList", trains);
                startActivity(intention);
            }
        });


        button01.setOnClickListener(find);
    }

    //populates the list view since its too difficult to do in the onclicklistener
    private void populate (String[] arr, String[] arr2)
    {
        ListView listView = (ListView) findViewById(R.id.mainListView);
        List<Map<String,String>> data = new ArrayList<Map<String,String>>();
        final Button buttonMap = (Button) findViewById(R.id.button2);

        //This hashmap is needed to populate the listView
        for (int j=0; j < arr.length; j++)
        {
            Map<String,String> data1 = new HashMap<String,String>(2);
            data1.put("train", arr[j]);
            data1.put("time", arr2[j]);
            data.add(data1);
        }

        //Displays the information into the listView
        SimpleAdapter ListAdapter = new SimpleAdapter(this, data,
                android.R.layout.simple_list_item_2,
                new String[] {"train", "time"},
                new int[] {android.R.id.text1,
                android.R.id.text2});

        listView.setAdapter(null);
        listView.setAdapter(ListAdapter);
        listView.setVisibility(View.VISIBLE);
        buttonMap.setVisibility(View.VISIBLE);

        //This onClickListener is responsible for the toast
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                String latestTrain = ((TextView)view.findViewById(android.R.id.text1)).getText().toString();
                lastStationAsynk last = new lastStationAsynk();
                last.execute(latestTrain);

                return true;
            }
        });
    }

    //Creates the toast to display on the screen
    private void trainBread(String lastStop, String schedTime, String actTime)
    {
        Toast.makeText(getBaseContext(), "Latest Stop: " + lastStop + "\nScheduled Arrival Time: " + schedTime + "\nActual Arrival Time: " + actTime, Toast.LENGTH_SHORT).show();
    }

    //Gets the train information and displays it in a listView using the "populate" function
    private class trainList extends AsyncTask<Void, Void, Void> {

        String[] time;

        @Override
        protected Void doInBackground(Void... arg0) {


            Spinner spinnerTop = (Spinner) findViewById(R.id.spinner1);
            String textTop = spinnerTop.getSelectedItem().toString();
            Spinner spinnerBottom = (Spinner) findViewById(R.id.spinner2);
            String textBottom = spinnerBottom.getSelectedItem().toString();

            textTop = textTop.replaceAll(" ", "%20");
            textBottom = textBottom.replaceAll(" ", "%20");

            String sURL = "http://www3.septa.org/hackathon/NextToArrive/" + textTop + "/" + textBottom + "/20";

            try {
                URL url = new URL(sURL);
                HttpURLConnection request = (HttpURLConnection) url.openConnection();
                request.connect();

                JsonParser jp = new JsonParser();
                JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
                JsonArray rootobj = root.getAsJsonArray();


                //The trainNumber and Time od arrival and departure start as lists so that items can be appended to the end
                List<String> trainNumber = new ArrayList<String>();
                List<String> Time = new ArrayList<String>();

                //For each entry in the array, get the trainNumber, and its arrival and departure times.  Arrival and Departure times are concatenated
                //into one string, in order to use one less array and make it easier to populate the listView
                for (int i = 0; i < rootobj.size(); i++) {
                    JsonObject item = rootobj.get(i).getAsJsonObject();
                    String train = item.get("orig_train").getAsString();
                    String depart = item.get("orig_departure_time").getAsString();
                    String arrival = item.get("orig_arrival_time").getAsString();

                    String times = "Depart: " + depart + "  Arrival: " + arrival;

                    trainNumber.add(train);
                    Time.add(times);
                }

                //Convert the lists from above into String arrays.  THese are passed into populate
                trains = trainNumber.toArray(new String[trainNumber.size()]);
                time = Time.toArray(new String[Time.size()]);

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void arg0) {
            //populate the listView with the train information
            populate(trains, time);
        }
    }	// async task


    //Get the last station the train was at.  Used in the "populate" function
    private class lastStationAsynk extends AsyncTask<String, Void, Void> {

        String latestStop = "";
        String scheduleTime = "";
        String actualTime = "";


        @Override
        protected Void doInBackground(String...lastTrain) {


            String sURL = "http://www3.septa.org/hackathon/RRSchedules/" + lastTrain[0];

            try {
                URL url = new URL(sURL);
                HttpURLConnection request = (HttpURLConnection) url.openConnection();
                request.connect();

                JsonParser jp = new JsonParser();
                JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
                JsonArray rootobj = root.getAsJsonArray();

                //Get the last station the train left from
                int i = 0;
                while(i < rootobj.size())
                {

                    JsonObject items = rootobj.get(i).getAsJsonObject();
                    //If it hasn't reached its destination, that means the last station is the one before.
                    //Since thats the case, break the while loop
                    if(items.get("act_tm").getAsString().equals("na"))
                    {
                        break;
                    }
                    else
                    {
                        latestStop = items.get("station").getAsString();
                        scheduleTime = items.get("sched_tm").getAsString();
                        actualTime = items.get("act_tm").getAsString();
                    }

                    i++;
                }


            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void arg0) {
            //Create the toast for the train
            trainBread(latestStop, scheduleTime, actualTime);
        }
    }	// async task



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

