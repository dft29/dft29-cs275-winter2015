package com.example.dana.gmaps;
//this file was generated form android's default gmap template.  Although, it had to be heavily modified

import android.content.Intent;
import android.location.Address;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import android.location.Geocoder;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    String[] train;
    ArrayList<LatLng> stations = new ArrayList<LatLng>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Intent intention = getIntent();
        train = intention.getStringArrayExtra("trainList");

        getStationName name = new getStationName();
        name.execute();

    }

    private class getStationName extends AsyncTask<Void, Void, Void> {

        String latestStop = "";
        @Override
        protected Void doInBackground(Void... arg0) {

            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            for (int k = 0; k < train[0].length(); k++)
            {
                String sURL = "http://www3.septa.org/hackathon/RRSchedules/" + train[k];
                latestStop = "";
                try {
                    URL url = new URL(sURL);
                    HttpURLConnection request = (HttpURLConnection) url.openConnection();
                    request.connect();

                    JsonParser jp = new JsonParser();
                    JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
                    JsonArray rootobj = root.getAsJsonArray();

                    int i = 0;
                    while (i < rootobj.size()) {

                        JsonObject items = rootobj.get(i).getAsJsonObject();
                        //IF it hasn't reached its destination, the last station is the one before
                        if (items.get("act_tm").getAsString().equals("na")) {
                            break;
                        } else {
                            latestStop = items.get("station").getAsString();
                        }

                        i++;
                    }
                    if(latestStop.equals(""))
                    {
                        //nothing
                    }
                    else
                    {
                        LatLng location = getLocation(latestStop + ", Philadelphia, PA");
                        stations.add(location);
                    }

                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void arg0) {
            setUpMap();
        }
    }

    //retrieves the latitude and longitude of the address entered
    public LatLng getLocation(String address) {
        Geocoder code = new Geocoder(this);
        List<Address> addressList;
        LatLng point1 = null;

        try {
            addressList = code.getFromLocationName(address, 5);
            if(addressList == null)
            {
                return null;
            }
            Address location = addressList.get(0);
            location.getLatitude();
            location.getLongitude();

            point1 = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return point1;
    }

    //adds the train markers to the map
    public void setUpMap() {
        ArrayList<Marker> markers = new ArrayList<Marker>();
        int i = 0;
        while (i < stations.size())
        {
            //creates a new marker with the position of the latitude and longitude, and gives it the name of the train
            Marker stationed = mMap.addMarker(new MarkerOptions().position(stations.get(i)).title(train[i]));
            markers.add(stationed);

            i++;
        }
    }
}
