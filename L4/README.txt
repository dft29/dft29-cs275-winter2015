Lab Completed by:
Dana Thompson: dft29
Zak Olyarnik:  zwo24

MainActivity.java:

	It checks to see if a database already exists.  If it does, if it has been accessed within an hour, then the app will display the information
from the database.  If its been more than an hour, then it will get the latitude and longitude using Android's GPS and run the Wunderground
Async task.  If a database has not been created, it will get the latitude and longitude using Android's GPS and run the Wunderground Async task.

	EXTRA CREDIT: The Android's GPS feature is used to ge the current location to make the call.

	The Async task has an onPostExecute command that will launch the populate function to fill in the list view.

Activity_main.xml:

	Its a list view

list_row.xml:

	contains an image view and four text views.  It serves as a custom layout for the list view in activity_main.xml

Weather.java:

	A custom class that is responsible for retrieving the information from the Json.  Has an overloaded getForecast function that either reads the
data from a Json array and stores the information into the database, or reads from the database and sends the info to the CustomArrayAdapter.

CustomArrayAdapter.java:

	Determines how list_row.xml will fill activity_main.xml.  An array that contains all the information about the forcast, and the appropriate text
views are populated with the information.  The image needs to be updated in an async task because the image's url is passed in, and requires a
network call.
The adapter's main structure came from https://github.com/codepath/android_guides/wiki/Using-an-ArrayAdapter-with-ListView
The image generation code came from http://stackoverflow.com/questions/4509912/is-it-possible-to-use-bitmapfactory-decodefile-method-to-decode-a-image-from-htt

DatabaseManager.java:

	Maintains the database.  Each piece of data that is requested is stored in a new column.  Most of the code was taken from the book, but
a custom removeAll and checkDatabase functions were added.  RetrievRows was modified to return a 2D array where each piece of relevant information
was parsed into a different index.


Notes:

	The code was written in Eclipse on my partners machine.  Since my laptop can't get android studio running, we decided to work on his laptop
because he could bring it to class.  We would both look up different parts for the lab, and the result ended up on his machine in Eclipse.  A week
ago, I imported it on my desktop that has Android Studio and it worked fine.  I ran it recently to record and it doesn't want to work now.  In
that time period, Android Studio had updated, and I believe that is the issue.  The error occurs when I try to feed the app coordinates.  I've tried rebuilding
the project, cleaning it, updating anything that needed updating, and nothing has worked.  I tried re-importing it and it still didn't work.  If you want
to see evidence of it working, look at my partner, Zak Olyarnik's, screencast.  He witnessed the project working in Android Studio on my Desktop, therefore,
he is my only alibi.
