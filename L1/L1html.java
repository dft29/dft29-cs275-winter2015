import java.net.*;  // for URL 
import java.io.*;
public class L1html {
	public static void main(String[] args) throws Exception {
		// opening up new URL
        URL laburl = new URL("https://www.cs.drexel.edu/~wmm24/cs275_wi15/labs/wxunderground.html");
        // reader
        BufferedReader in = new BufferedReader(
        // look up the content in URL
        new InputStreamReader(laburl.openStream()));
        // read the html content from the reader and save it to a string
        String htmlDisplay;
        //print out the html
        while ((htmlDisplay = in.readLine()) != null)
            System.out.println(htmlDisplay);
        in.close();
    }
}
