
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
// Requires gson jars
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class L1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		// Get from http://www.wunderground.com/weather/api/
				String key;
				if(args.length < 1) {
					System.out.println("Enter key: ");
					
					Scanner in = new Scanner(System.in);
					key = in.nextLine();
				} else {
					key = args[0];
				}
				
				String sURL = "http://api.wunderground.com/api/" + key + "/geolookup/q/autoip.json";
				
				// Connect to the URL
				URL url = new URL(sURL);
				HttpURLConnection request = (HttpURLConnection) url.openConnection();
				request.connect();
				
				// Convert to a JSON object to print data
		    	JsonParser jp = new JsonParser();
		    	JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
		    	JsonObject rootobj = root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive
		    	
		    	// Get some data elements and print them
		    	String city = rootobj.get("location").getAsJsonObject().get("city").getAsString();
		    	String state = rootobj.get("location").getAsJsonObject().get("state").getAsString();
		    	int zip1 = rootobj.get("location").getAsJsonObject().get("zip").getAsInt();
		    	double lat = rootobj.get("location").getAsJsonObject().get("lat").getAsDouble();
		    	double lon = rootobj.get("location").getAsJsonObject().get("lon").getAsDouble();
				System.out.println(zip1);
				System.out.println(city);
				System.out.println(state);
				System.out.println("lat: " + lat + "  lon: " + lon);
				
				 sURL = "http://api.wunderground.com/api/" + key + "/hourly/q/" + state + "/" + city + ".json";
				
				 //redo the url with the city and state saved from the auto lookup
                url = new URL(sURL);
                request = (HttpURLConnection) url.openConnection();
                request.connect();
                
                jp = new JsonParser();
                root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
                rootobj = root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive
                
                //create the array from the json.
                JsonArray timearray = rootobj.get("hourly_forecast").getAsJsonArray();
                
                for( int i = 0; i < timearray.size(); i++) {
                    JsonObject time = timearray.get(i).getAsJsonObject();
                    
                    //grab relevant fields.
                    String pretty = time.get("FCTTIME").getAsJsonObject().get("pretty").getAsString();
                    String temp = time.get("temp").getAsJsonObject().get("english").getAsString();
                    String cond = time.get("condition").getAsString();
                    String humid = time.get("humidity").getAsString();
                    
                    //prints them out.
                    System.out.println(pretty);
                    System.out.println(">\t" + temp);
                    System.out.println(">\t" + cond);
                    System.out.println(">\t" + humid);
                    System.out.println();
                    
                } 
	}

}
